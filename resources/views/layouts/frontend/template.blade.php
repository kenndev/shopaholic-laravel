<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Acele - App Landing PSD Template">
        <meta name="keywords" content="Acele, responsive, app, landing page, company, creative, android, ios, iphone, html template">
        <meta name="author" content="SposobStudio">
        <title>{{getcong('site_name')}}</title>
        <link rel="stylesheet" href="{{asset('/packages/frontend/vendors/bootstrap/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('/packages/frontend/vendors/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('/packages/frontend/vendors/stroke-gap-icons/style.css')}}">
        <link rel="stylesheet" href="{{asset('/packages/frontend/vendors/magnific-popup/magnific-popup.css')}}">
        <link rel="stylesheet" href="{{asset('/packages/frontend/vendors/animate/animate.css')}}">
        <link rel="stylesheet" href="{{asset('/packages/frontend/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('/packages/frontend/css/font-awesome.min.css')}}">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="top_page">

        <!-- Loader -->
        <div class="loader">
            <div class="spinner">
                <div class="double-bounce1"></div>
            </div>
        </div>

        <!-- Main Header -->
        <header id="main_header">
            <div class="logo">
                <a href="#">
                    <img src="{{asset('packages/frontend/img/logo_white.png')}}" class="logo_white" alt="Acele">
                    <img src="{{asset('packages/frontend/img/logo_black.png')}}" class="logo_black" alt="Acele">
                </a>
            </div>
            <div class="menu">
                <ul class="nav">
                    <li><a href="#top_page">home</a></li>
                    <li><a href="#features">features</a></li>
                    <li><a href="#screenshots">screenshots</a></li>

                    <li><a href="#download">download</a></li>
                    <li><a href="#contact">contact</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </header>

        <!-- Mobile Header -->
        <header id="mobile_header">
            <div class="logo">
                <a href="#">
                    <img src="{{asset('packages/frontend/img/logo_white.png')}}" alt="Uchaguzi254">
                </a>
            </div>
            <div class="menu_toggle">
                <a href="#">
                    <i class="fa fa-bars"></i>
                    <i class="fa fa-close"></i>
                </a>
            </div>

            <div class="clearfix"></div>

            <div class="menu">
                <ul>
                    <li><a href="#top_page">home</a></li>
                    <li><a href="#features">features</a></li>
                    <li><a href="#screenshots">screenshots</a></li>
                    <li><a href="#download">download</a></li>
                    <li><a href="#contact">contact</a></li>
                </ul>
            </div>
        </header>

        <!-- Add Padding Top -->
        <!-- <div class="clear_heading"></div> -->

        <!-- Hero Section -->
        <section id="hero">
            <div class="overlay"></div>
            <div class="container">
                <div class="row content">
                    <div class="col-md-5 vam ui_wrapper">
                        <img src="{{asset('packages/frontend/img/hero_ui.png')}}" class="animated" alt="ui">
                    </div>
                    <div class="col-md-7 vam text">
                        <h3 style="font-family:Courier; color:white;">Uchaguzi254</h3>
                        <h4 class="animated">Uamuzi Ni Wako</h4>
                        <p class="description animated">{{$leadText->text}}</p>
                        <!--<a target="_blank" href="https://play.google.com/store/apps/details?id=com.uchaguzi" class="android btn_acele btn_white"><i class="fa fa-android"></i>Download</a>-->
                        <a target="_blank" href="#download" class="btn_acele btn_white animated">download</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Features Section -->
        <section id="features">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 vab">
                        <h3 class="animated">Uchaguzi254 features</h3>

                    </div>
                </div>
                @foreach($features->chunk(4) as $chuck)
                <div class="row" style="margin-top: 50px;">
                    @foreach($chuck as $row)
                    <div class="col-md-3 vab feature">
                        <i class="icon {{$row->icon}}"></i>
                        <h6>{{$row->text}}</h6>

                    </div>
                    @endforeach

                </div>
                @endforeach

            </div>
        </section>



        <!-- ScreenShoots Section -->
        <section id="screenshots">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="animated">screenshots</h3>
                        <div class="cycle-slideshow"
                             data-cycle-fx="fade" 
                             data-cycle-swipe="true"
                             data-cycle-timeout="7000"
                             data-cycle-slides="> div"
                             data-cycle-pager=".bullets_screenshots"
                             >
                            @foreach($screenshots as $row)
                            <div class="row screen">
                                <div class="col-md-6 vam">
                                    <h5>{{$row->title}}</h5>
                                    <p>{{$row->text}}</p>

                                </div>
                                <div class="col-md-6 vam image">
                                    <img src="{{asset('packages/uploads/screenshots/'.$row->image_url)}}" alt="screen">
                                </div>
                            </div>
                            @endforeach


                        </div>

                        <div class="bullets bullets_screenshots"></div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Download Section -->
        <section id="download">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h4>Download now Uchaguzi254 App!</h4>
                        <p>Click the link below to join the fasted growing network in Kenya today.</p>

                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.uchaguzi" class="android btn_acele btn_stroke_white"><i class="fa fa-android"></i>google play</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Contact Section -->
        <section id="contact">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="animated">contact us</h3>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-4 contact_info">
                        <div class="info phone">





                            <i class="icon icon-Phone2"></i>
                            <h5>phone</h5>
                            <p>(+254) 718933367</p>
                            <p>(+254) 704103356</p>
                        </div>
                        <div class="info location">
                            <i class="icon icon-Pointer"></i>
                            <h5>location</h5>
                            <p>Vedic House, 2 nd Floor, RM 205,</p>
                            <p>Mama Ngina Street,</p>
                            <p>P. O. Box. 12945 – 00400,</p>
                            <p>Nairobi, Kenya.</p>
                        </div>
                        <div class="info email">
                            <i class="icon icon-Mail"></i>
                            <h5>email</h5>
                            <p>info@uchaguzi254.co.ke</p>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div id="success"></div>
                        <form id="formcontact" method="POST" class="contact_form">
                            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" id="name" class="name" name="name" placeholder="First name">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" id="email" class="email" name="email" placeholder="Email address">
                                </div>
                                <div class="col-md-12">
                                    <textarea name="message" id="message" cols="30" rows="10" placeholder="Message ..."></textarea>
                                </div>
                            </div>

                            <div>
                                <div class="col-md-6 col-md-offset-2">
                                    <button type="submit" class="btn_acele btn_primary">SUBMIT</button>
                                </div>
                                <div class="col-md-3 pull-left">
                                    <div class="status-progress pull-left">
                                        <img style="margin-right: 10px; padding-right: 10px;" src="{{URL::asset('packages/frontend/loader/loader.gif')}}"/>
                                    </div>
                                </div>


                            </div>


                        </form>

                    </div>


                </div>
            </div>
        </section>

        <!-- Footer -->
        <footer id="main_footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="info">
                            <a href="#">
                                <img src="{{asset('packages/frontend/img/logo_white.png')}}" class="logo_white" alt="Acele">
                            </a>
                            <p>{{getcong('description')}}</p>
                        </div>

                        <div class="social_icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>

                            </ul>
                        </div>
                        <div class="copyright">
                            <p>© {{date('Y')}} {{getcong('site_name')}}. Designed & Developed with <i class="fa fa-heart"></i>  by <a href="http://designlabtechnologies.com">Design Lab Technologies</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Go To Top -->
        <div id="go_to_top"><a href="#top"><i class="fa fa-angle-up"></i></a></div>

        <script src="{{asset('packages/frontend/vendors/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('packages/frontend/vendors/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{asset('packages/frontend/vendors/cycle2/jquery.cycle2.min.js')}}"></script>
        <script src="{{asset('packages/frontend/vendors/cycle2/jquery.cycle2.swipe.min.js')}}"></script>   
        <script src="{{asset('packages/frontend/vendors/waypoints/waypoints.min.js')}}"></script>
        <script src="{{asset('packages/frontend/vendors/magnific-popup/magnific-popup.min.js')}}"></script>
        <script src="{{asset('packages/frontend/js/main.js')}}"></script>
        <script>
$(document).ready(function () {
    $(".status-progress").hide();
});

var base_url = '{{url()}}';
$('#formcontact').submit(function (e) {

    e.preventDefault();
    $("#submit").addClass("disabled");
    $(".status-progress").show();

    //Creating an ajax method
    $.ajax({
        //Getting the url of the uploadphp from action attr of form 
        //this means currently selected element which is our form 
        url: base_url + "/sendMail",
        //For file upload we use post request
        type: "POST",
        //Creating data from form 
        data: new FormData(this),
        //Setting these to false because we are sending a multipart request
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            //If the request is successfull we will get the scripts output in data variable 
            //Showing the result in our html element 
            $("#submit").removeClass("disabled");

            $(".status-progress").hide();
            $('#name').val('');
            $('#email').val('');
            $('#message').val('');
            $('#success').html(data);
        },
        error: function () {}
    });
});
        </script>
    </body>
</html>