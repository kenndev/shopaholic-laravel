<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li {{ isActiveRoute2('home') }}><a href="{{route('home')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
            <li {{ isActiveRoute2('slider') }}><a href="{{route('slider')}}"><i class="glyphicon glyphicon-camera"></i> <span>Slider</span></a></li>
            <li {{ isActiveRoute2('category') }}><a href="{{route('category')}}"><i class="glyphicon glyphicon-bishop"></i> <span>Category</span></a></li>
            <li {{ isActiveRoute2('products') }}><a href="{{route('products')}}"><i class="glyphicon glyphicon-cd"></i> <span>Products</span></a></li>
            
            <li>
                <a href="#"><i class="glyphicon glyphicon-yen"></i> <span>Orders</span></a>
                <ul>

                    <li {{ isActiveRoute2('orders.view') }}><a href="{{route('orders.view')}}">All orders</a></li>
                    <li {{ isActiveRoute2('orders.ordered') }}><a href="{{route('orders.ordered')}}">Orders ordered</a></li>
                    <li {{ isActiveRoute2('orders.shipped') }}><a href="{{route('orders.shipped')}}">Orders shipped</a></li>
                    <li {{ isActiveRoute2('orders.received') }}><a href="{{route('orders.received')}}">Orders received</a></li>
                    

                </ul>
            </li>
            
            
            <li {{ isActiveRoute2('adverts') }}><a href="{{route('adverts')}}"><i class="glyphicon glyphicon-baby-formula"></i> <span>adverts</span></a></li>

            <li>
                <a href="#"><i class="icon-cog3"></i> <span>Settings</span></a>
                <ul>

                    <li {{ isActiveRoute2('settings') }}><a href="{{route('settings')}}">Settings</a></li>
                    <li {{ isActiveRoute2('colors') }}><a href="{{route('colors')}}">Colors</a></li>
                    <li {{ isActiveRoute2('size') }}><a href="{{route('size')}}">Size</a></li>
                </ul>
            </li>



        </ul>
    </div>
</div>
