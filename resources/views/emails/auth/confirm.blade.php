<h1>{{ Lang::get('confide::confide.email.account_confirmation.subject') }}</h1>

<p>{{ Lang::get('confide::confide.email.account_confirmation.greetings', array('name' => (isset($asp['name'])) ? $asp['name'] : $asp['email'])) }},</p>

<p>{{ Lang::get('confide::confide.email.account_confirmation.body') }}</p>
<a href='{{{ URL::to("aspirants/confirm/{$asp['confirmation_code']}") }}}'>
    {{{ URL::to("aspirants/confirm/{$asp['confirmation_code']}") }}}
</a>

<p>{{ Lang::get('confide::confide.email.account_confirmation.farewell') }}</p>
