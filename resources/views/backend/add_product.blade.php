@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Products</span> - Add</h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{route('products')}}">Products</a></li>
                <li class="active">Add a new product</li>
            </ul>


        </div>


    </div>

    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
        @endif

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <form autocomplete="off" role="form" method="POST" action="{{ route('products.add') }}"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="id" name="inputId"/>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="control-label" for="inputProductName">Product Name</label>
                            <input type="text" class="form-control" id="inputProductName" name="inputProductName"
                                   placeholder="Product Name" value="{{ old('inputProductName') }}"/>
                            @if ($errors->has('inputProductName'))
                                <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('inputProductName') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label class="control-label" for="imageFile">Upload Image</label>
                            <input type="file" class="file-input" data-show-caption="true" data-show-upload="false"
                                   id="imageFile" name="imageFile"
                                   placeholder="Upload an Image" value="{{ old('imageFile') }}"/>
                            @if ($errors->has('imageFile'))
                                <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('imageFile') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label class="control-label">Category </label>
                            <select id="selectCategory" name="selectCategory" class="select-search">
                                @foreach ($category as $row)
                                    <option value="{{$row->id}}">{{$row->category_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('selectCategory'))
                                <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('selectCategory') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Units available</label>
                            <input type="text" class="form-control" id="inputUnitsAvailable" name="inputUnitsAvailable"
                                   placeholder="Enter units available" value="{{ old('inputUnitsAvailable') }}"/>
                            @if ($errors->has('inputUnitsAvailable'))
                                <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('inputUnitsAvailable') }}</strong>
                        </span>
                            @endif
                        </div>

                    </div>


                    <div class="shoes box">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label class="control-label">Size </label>
                                <select id="size" name="size[]" class="select" multiple="multiple">
                                    <optgroup label="Select size">
                                        @foreach($size as $row)
                                            <option value="{{$row->size}}">{{$row->size}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Colors available</label>
                                <select id="colors" name="colors[]" class="select" multiple="multiple">
                                    <optgroup label="Select color">
                                        @foreach($colors as $row)
                                            <option value="{{$row->color_name}}">{{$row->color_name}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label class="control-label">Old Price </label>
                            <input type="text" class="form-control" id="inputOldPrice" name="inputOldPrice"
                                   placeholder="Old Price" value="{{ old('inputOldPrice') }}"/>
                            @if ($errors->has('inputOldPrice'))
                                <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('inputOldPrice') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">New Price </label>
                            <input type="text" class="form-control" id="inputPrice" name="inputPrice"
                                   placeholder="New Price" value="{{ old('inputPrice') }}"/>
                            @if ($errors->has('inputPrice'))
                                <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('inputPrice') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">

                        <div class="col-md-12">
                            <label class="control-label" for="inputProductDescription">Description</label>
                            <textarea rows="4" cols="4" type="text" class="form-control" id="inputProductDescription"
                                      name="inputProductDescription"
                                      placeholder="Description" value="{{ old('inputProductDescription') }}"></textarea>
                            @if ($errors->has('inputProductDescription'))
                                <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('inputProductDescription') }}</strong>
                        </span>
                            @endif
                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary btn-lg">Save changes</button>
                </form>

            </div>


        </div>
        <!-- /basic datatable -->

        @include("layouts.backend.footer")
    </div>


@endsection
