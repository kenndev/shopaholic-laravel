@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Color</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Color</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Colors</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="javascript: ajaxmodal()" class="btn btn-outline btn-primary"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add a Color</a>

        </div>


        <table class="table table-hover table-condensed table-bordered table-striped" id="category-table">
            <thead>
                <tr>
                  
                    <th>Color</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->


    <script>
        $(document).ready(function () {
            oTable = $('#category-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('colors.table') !!}',
                "columns": [
                    {data: 'color_name', name: 'color_name'},
                    {data: 'action', name: 'action', 'searchable': false}
                ]
            });
        });
                function ajaxmodaledit(id) {
                    var url2 = base_url + '/admin/colors-details';
                   
                            $.get(url2 + '/' + id, function (data) {
                                $('#id').val(data.id);
                                $('#color_name').val(data.color_name);
                                $('#exampleNiftySideFall').modal('show');
                            });
                }
    </script>
    <script>
        function checkDelete()
        {
            var chk = confirm("Are You Sure To Delete This !");
            if (chk)
            {
                return true;
            } else {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#color_name').val("");
            $('#exampleNiftySideFall').modal('show');
        }
    </script>


    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Modal -->
    <div class="modal fade modal-slide-in-right" id="exampleNiftySideFall" aria-hidden="true"
         aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Colors</h4>
                </div>
                <div class="modal-body">
                    <!-- Example Basic Form -->

                    <form autocomplete="off" role="form" method="POST" action="{{ route('colors.add') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" class="form-control" id="id" name="inputId"/>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="control-label" for="inputBasicFirstName">Color Name</label>
                                <input type="text" class="form-control" id="color_name" name="color_name"
                                       placeholder="Color Name" value="{{ old('color_name') }}" required/>
                                @if ($errors->has('color_name'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('color_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        

                        <!-- End Example Basic Form -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->



</div>
<!-- /content area -->
@endsection


