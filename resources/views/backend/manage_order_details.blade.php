@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Order number: {{$orderIdAlphanumeric}}</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{route('orders.view')}}">All Orders</a></li>
                <li class="active">Order details</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Order details</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
            <!--<a href="{{route('product.new.add')}}" class="btn btn-outline btn-primary"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add a product</a>-->

            </div>


            <table class=" table table-hover table-striped dataTable width-full" id="category-table">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Category name</th>
                    <th>Product name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /basic datatable -->


        <script>
            $(document).ready(function () {

                $('#category-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: base_url + "/admin/ordersdetailsTable/" + '{{$orderid}}',
                    columns: [
                        {
                            data: 'image',
                            name: 'products.image',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                if (data) {
                                    var filename = data;
                                    return '<img src="' + filename + '" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                } else {
                                    return '<img src="{!! asset("packages/backend/assets/photos/placeholder.png") !!}" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                }
                            }
                        },
                        {data: 'category_name', name: 'category.category_name'},
                        {data: 'productName', name: 'products.productName'},
                        {data: 'price', name: 'products.price'},
                        {data: 'quantity', name: 'quantity', orderable: false, searchable: false},
                        {
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                var total = full['quantity'] * full['price'];
                                return total
                            }
                        },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            });

            function ajaxDetails(id) {
                document.getElementById("size").innerHTML = "";
                document.getElementById("color").innerHTML = "";
                var url2 = '{!! url('admin/order-details-items') !!}';
                $.get(url2 + '/' + id, function (data) {
                    document.getElementById("size").innerHTML = data.siz_e;
                    document.getElementById("color").innerHTML = data.color;
                    $('#exampleNiftySideFall').modal(data);
                });
            }
        </script>

        <div class="row ">

        </div>

        <br>


        <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


        <!-- Modal -->
        <div class="modal fade modal-slide-in-right" id="exampleNiftySideFall" aria-hidden="true"
             aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Details</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>Size</th>
                                <th><p id="size"></p></th>
                            </tr>
                            <tr>
                                <th>Color</th>
                                <th><p id="color"></p></th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->


    </div>
    <!-- /content area -->
@endsection




