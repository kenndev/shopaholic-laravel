@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Orders Received</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{route('orders.view')}}">All Orders</a></li>
                <li class="active">Orders Received</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Orders Received</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <table class="table table-hover table-condensed table-bordered table-striped" id="category-table">
                <thead>
                <tr>
                    <th>Customer</th>
                    <th>Phone</th>
                    <th>No of products</th>
                    <th>Sum</th>
                    <th>Order number</th>
                    <th>Date</th>
                    <th>Order status</th>
                    <th>Payment status</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /basic datatable -->


        <script>
            $(document).ready(function () {
                oTable = $('#category-table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": '{!! route('orders.received.Table') !!}',
                    "columns": [

                        {
                            data: 'first_name',
                            name: 'first_name',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {

                                if (data == null && full['last_name'] == null) {
                                    return full['email'];
                                } else {
                                    return data + " " + full['last_name'];
                                }
                            }
                        },

                        {data: 'contact', name: 'contact', 'searchable': false},
                        {data: 'no_of_items', name: 'no_of_items', 'searchable': false},
                        {data: 'totalAmount', name: 'totalAmount', 'searchable': false},
                        {data: 'order_id', name: 'order_id'},
                        {data: 'check_out_date', name: 'check_out_date'},
                        {
                            data: 'order_status',
                            name: 'order_status',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                return '<span class="label label-default">' + data + '</span>';
                            }
                        },
                        {
                            data: 'paid_or_not',
                            name: 'paid_or_not',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                if (data == 0) {

                                    return '<span class="label label-warning">Not paid</span>';
                                } else {
                                    return '<span class="label label-success">Paid</span>';
                                }
                            }
                        },
                        {data: 'action', name: 'action', 'searchable': false}
                    ]
                });
            });

        </script>

        <div class="row ">

        </div>

        <br>


        <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->

    </div>
    <!-- /content area -->
@endsection


