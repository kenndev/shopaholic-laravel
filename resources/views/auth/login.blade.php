@extends('layouts.layout')

@section('content')


    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="login-form">
            <div class="text-center">
                <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                <h5 class="content-group-lg">Login to your account
                    <small class="display-block">Enter your credentials</small>
                </h5>
            </div>

            <div class="form-group has-feedback has-feedback-left">
                {{--<input type="text" class="form-control input-lg" placeholder="Username">--}}
                <input id="email" type="email" placeholder="email" class="form-control input-lg" name="email"
                       value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
            </div>

            <div class="form-group has-feedback has-feedback-left">
                {{--<input type="password" class="form-control input-lg" placeholder="Password">--}}
                <input id="password" placeholder="Password" type="password" class="form-control input-lg" name="password" required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong style="color: red">{{ $errors->first('password') }}</strong>
                                    </span>
                @endif

            </div>

            <div class="form-group login-options">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            {{--<input type="checkbox" class="styled" checked="checked">--}}

                        </label>
                    </div>

                    <div class="col-sm-6 text-right">
                        <a href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn bg-blue btn-block btn-lg">Login <i
                            class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>


@endsection