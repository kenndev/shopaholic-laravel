<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model {

    protected $fillable = [];
    protected $table = 'order_details';

}
