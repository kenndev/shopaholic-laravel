<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsImages extends Model {

    protected $fillable = [];
    protected $table = 'product_images';

}
