<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckoutDetails extends Model {

    protected $fillable = [];
    protected $table = 'checkout_details';

}
