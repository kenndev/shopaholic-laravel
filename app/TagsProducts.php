<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagsProducts extends Model
{
    protected $fillable = [];
    protected $table = 'tags_products';
}
