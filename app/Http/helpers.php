<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function isActiveRoute($route) {
    return Request::is($route) ? 'class=active' : '';
}

function isActiveRoute2($route) {
    if (strcmp(Route::currentRouteName(), $route) == 0) {
        return 'class=active';
    }
}



function isActiveFrontRoute($route) {
    return Request::is($route) ? 'class=selected' : '';
}

function insertimage() {

    if (Auth::user()->image_url) {
        ?>
        <a href = "#" class = "media-left"><img src = "<?php echo URL::asset('packages/uploads/images/logo/size200/' . Auth::user()->image_url) ?>" class = "img-circle img-sm" alt = ""></a>
    <?php } else { ?>

        <a href = "#" class = "media-left"><img src = "<?php echo URL::asset('packages/backend/assets/images/placeholder.jpg') ?>" class = "img-circle img-sm" alt = ""></a>

        <?php
    }
}

if (!function_exists('getcong')) {

    function getcong($key) {

        $settings = \App\Settings::first();

        return $settings->$key;
    }

}
