<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class MobileController extends Controller
{


    public function getAllProducts()
    {
        $new = \App\Product::leftJoin('category', 'products.category_id', '=', 'category.id')
            ->select('products.rate', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription',  'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
            ->where('products.activated', 1)
            ->inRandomOrder()
            ->get();
        $response["result"] = "success";
        $response["newProduct"] = $new;
        echo json_encode($response);
    }

    public function getCategorys()
    {
        $category = \App\Category::select('image', 'category_name', 'id as categoryId')->get();
        $productCountArray = new Collection();
        $myArray = array();
        foreach ($category as $row) {
            $productsCount = \App\Product::select(\DB::raw('COUNT(category_id) as count'))
                ->where('category_id', $row->categoryId)
                ->get();
            $P = array("count" => $productsCount[0]['count'], "categoryId" => $row->categoryId);
            array_push($myArray, $P);

            $productCountArray = $productCountArray->merge($productsCount);
        }
        $response["result"] = "success";
        $response["category"] = $category;
        $response["product_count"] = $myArray;
        return response()->json($response);
    }

    public function getCategorysProducts()
    {
        $category = \App\Category::select('image', 'category_name', 'id as categoryId')->take(5)->get();
        $productArray = new Collection();
        foreach ($category as $row) {
            $prducts_reslts = $this->getProductsByCategorypro($row->categoryId);
            $productArray = $productArray->merge($prducts_reslts);

        }
        $response["result"] = "success";
        $response["category"] = $category;

        $response["products"] = $productArray;

        return response()->json($response);
    }

    public function getProductsByCategorypro($category_id)
    {
        $products = \App\Product::leftJoin('category', 'products.category_id', '=', 'category.id')
            ->select('products.rate', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription',  'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
            ->where('products.category_id', $category_id)
            ->take(10)
            ->get();
        return $products;
    }

    public function getProductsByCategory(Request $request)
    {
        $category_id = $request->input('category_id');
        $products = \App\Product::leftJoin('category', 'products.category_id', '=', 'category.id')
            ->select('products.rate', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription',  'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
            ->where('products.category_id', $category_id)
            ->where('products.activated', 1)
            ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        return response()->json($response);
    }

    public function searchProducts(Request $request)
    {
        $search_term = $request->input('search_term');
        $products = \App\Product::leftJoin('category', 'products.category_id', '=', 'category.id')
            ->where('productName', 'like', '%' . $search_term . '%')
            ->select('products.rate', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription',  'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
            ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        return response()->json($response);
    }


    //Get product images
    public function getProductImages(Request $request)
    {
        $product_id = $request->input('product_id');
        $productimages = \App\ProductsImages::select('image_url')->where('product_id', $product_id)->get();
        if (count($productimages)) {
            $response["images"] = $productimages;
        } else {
            $productsimage2 = \App\Product::select('image as image_url')->where('id', $product_id)->get();
            $response["images"] = $productsimage2;
        }
        $response["result"] = "success";

        echo json_encode($response);
    }

    //Get order history
    public function orderHistory(Request $request)
    {
        $shopper_id = $request->input('firebase_id');
        $shopper = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $history = \App\Checkout::where('shopper_id', $shopper->id)
            ->leftJoin('order_status', 'checkout.order_status', '=', 'order_status.id')
            ->selectRaw('checkout.order_id,checkout.totalAmount,checkout.payment_option,order_status.order_status,checkout.no_of_items,checkout.id as checkOutID,checkout.check_out_date')
            ->orderBy('checkout.id', 'DESC')
            ->get();


        if (count($history)) {
            $response["result"] = "success";
            $response["message"] = "Order history retrieved successfully";
            $response["orders"] = $history;

        } else {
            $response["result"] = "failure";
            $response["message"] = "no order history at the moment.";

        }
        return response()->json($response);
    }

    /*
     * 
     * Get one shipping address and one billing address
     */
    public function getShippingAndBillingAddress(Request $request)
    {
        $shopper_id = $request->input('firebase_id');
        $user = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $shipping = \App\ShippingAdress::where('user_id', $user->id)->first();
        $billing = \App\Billing::where('user_id', $user->id)->first();
        if (count($shipping)) {
            $response["shipping"] = $shipping;
            $response["shippingempty"] = false;
        } else {
            $response["shippingempty"] = true;
        }
        if (count($billing)) {
            $response["billingempty"] = false;
            $response["billing"] = $billing;
        } else {
            $response["billingempty"] = true;
        }
        $response["result"] = "success";
        $response["message"] = "Shipping address retrieved successfully";
        return response()->json($response);
    }

    /**
     * Get a json object of addresses both billing and shiping address
     */
    public function getAddress(Request $request)
    {
        $shopper_id = $request->input('firebase_id');
        $user = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $shipping = \App\ShippingAdress::where('user_id', $user->id)->get();
        $billing = \App\Billing::where('user_id', $user->id)->get();
        if (count($shipping)) {
            $response["shipping"] = $shipping;
            $response["message"] = "Address retrieved successfully";
            $response["shippingempty"] = false;
        } else {
            $response["shippingempty"] = true;
        }
        if (count($billing)) {
            $response["billingempty"] = false;
            $response["message"] = "Address retrieved successfully";
            $response["billing"] = $billing;
        } else {
            $response["billingempty"] = true;
        }
        $response["result"] = "success";

        return response()->json($response);
    }

    public function saveShippingAdress(Request $request)
    {
        $first_name = $request->input('first_name');
        $address_id = $request->input('address_id');
        $last_name = $request->input('last_name');
        $phone_number = $request->input('phone_number');
        $post_code = $request->input('post_code');
        $state = $request->input('state');
        $town = $request->input('town');
        $country = $request->input('country');
        $address = $request->input('address');
        $shopper_id = $request->input('firebase_id');
        $user = \App\Shoppers::where('firebase_id', $shopper_id)->first();

        if ($address_id = !"") {
            $check_if_shipping_address_exists = \App\ShippingAdress::where('same_as_billing', $address_id)->first();
        }

        if (count($check_if_shipping_address_exists)) {
            $response["result"] = "failure";
            $response['message'] = 'Record already exists. Add another shipping address';
        } else {
            $shippingAdd = new \App\ShippingAdress();
            $shippingAdd->user_id = $user->id;
            $shippingAdd->first_name = $first_name;
            $shippingAdd->last_name = $last_name;
            $shippingAdd->phone_number = $phone_number;
            $shippingAdd->country = $country;
            $shippingAdd->state = $state;
            $shippingAdd->town = $town;
            $shippingAdd->postcode = $post_code;
            $shippingAdd->address = $address;
            $shippingAdd->save();
            $response["result"] = "success";
            $response["message"] = "Shipping address saved successfully";
        }
        return response()->json($response);
    }

    public function saveBillingAdress(Request $request)
    {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $phone_number = $request->input('phone_number');
        $post_code = $request->input('post_code');
        $state = $request->input('state');
        $town = $request->input('town');
        $country = $request->input('country');
        $address = $request->input('address');
        $shopper_id = $request->input('firebase_id');
        $user = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $billing = new \App\Billing();
        $billing->user_id = $user->id;
        $billing->first_name = $first_name;
        $billing->last_name = $last_name;
        $billing->phone_number = $phone_number;
        $billing->country = $country;
        $billing->state = $state;
        $billing->town = $town;
        $billing->postcode = $post_code;
        $billing->address = $address;
        $billing->save();

        if (count($billing)) {
            $response["result"] = "success";
            $response["message"] = "Billing address saved successfully";
        } else {
            $response["result"] = "failure";
            $response["message"] = "Billing address was not saved";
        }
        return response()->json($response);
    }

    /**
     * Login New Shopper
     */
    public function saveProfile(Request $request)
    {

        $firebase_id = $request->input('firebase_id');
        $email = $request->input('email');
        $user = \App\Shoppers::where('firebase_id')->first();
        if (!count($user)) {
            $myuser = new \App\Shoppers();
            $myuser->email = $request->input('email');
            $myuser->firebase_id = $request->input('firebase_id');
            $saved = $myuser->save();
        }

        if ($saved) {
            $response["result"] = "success";
            $response["message"] = "User created successfully";
            return response()->json($response);
        } else {
            $response["result"] = "failure";
            $response["message"] = "User Data has synced successfully";
            return response()->json($response);
        }

    }

    //Update user profile
    public function updateShopper(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $firebase_id = $request->input('firebase_id');
        $contact = $request->input('contact');
        $shoppers2 = \App\Shoppers::where('firebase_id', $firebase_id)->first();
        $shoppers2->email = $email;
        $shoppers2->name = $name;
        $shoppers2->contact = $contact;
        $shoppers2->save();
        if ($shoppers2->id) {
            $response["result"] = "success";
            $response["message"] = "User Profile updated successfully";
            return response()->json($response);
        } else {
            $response["result"] = "failure";
            $response["message"] = "Error updating user profile";
            return response()->json($response);
        }
    }

    //Update profile pic
    public function updateProfilePic(Request $request)
    {
        $firebase_id = $request->input('firebase_id');
        $shoppers2 = \App\Shoppers::where('firebase_id', $firebase_id)->first();
        $destinationPath = 'packages/uploads/profile/' . \Carbon\Carbon::now()->year . '/' . \Carbon\Carbon::now()->month;
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        $filename = \Carbon\Carbon::now()->year . '_' . \Carbon\Carbon::now()->month . '_' . \Carbon\Carbon::now()->day . '_' . time() . '.png';
        $base64 = $request->input('file');

        // This saves the base64encoded destinationPath
        $upload_success = file_put_contents($destinationPath . '/' . $filename, base64_decode($base64));
        if ($upload_success) {
            $response["result"] = "success";
            $response["message"] = "Profile picture updated successfully";
            return response()->json($response);
        } else {
            $response["result"] = "failure";
            $response["message"] = "Error updating profile picture";
            return response()->json($response);
        }
    }

    /**
     * Get Slider
     */
    public function slider()
    {
        $slider = \App\Slider::leftJoin('products', 'slider.product_id', '=', 'products.id')
            ->leftJoin('category', 'products.category_id', '=', 'category.id')
            ->select('slider.image_url','products.rate', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
            ->get();
        $response["result"] = "success";
        $response["slider"] = $slider;
        echo json_encode($response);
    }

    /**
     * Check out
     */
    public function checkOut(Request $request)
    {
        $order_id = $request->input('order_id');
        $totalAmount = $request->input('totalAmount');
        $stripeToken = $request->input('stripeToken');
        $firebase_id = $request->input('firebase_id');
        $payment_option = $request->input('payment_option');
        $payment_id = $request->input('payment_id');
        $order_details = $request->input('order_details');
        $no_of_items = $request->input('no_of_items');
        $checkout_date = $request->input('check_out_date');

        $details = json_decode($order_details, true);
        $shopper = \App\Shoppers::where('firebase_id', $firebase_id)->first();

        if ($payment_option == 'Stripe') {
            $payment_id_generated = "stripe-" . str_random(8);

            \Stripe\Stripe::setApiKey('sk_test_QWqVmu7QJWp6LusRVGotkVnu');
            try {
                \Stripe\Charge::create(array(
                    "amount" => $totalAmount,
                    "currency" => "usd",
                    "source" => $stripeToken,
                    "description" => "Test payment."
                ));

                $paid_or_not = 1;

                $this->insertCheckOut($paid_or_not,$order_id, $totalAmount, $shopper->id, $payment_option, $payment_id_generated, $details, $no_of_items, $checkout_date);

                $response['result'] = 'success';
                $response['message'] = 'Payment processed successfully. Thank you and continue shopping.';
                return response()->json($response);
            } catch (\Exception $e) {
                $response['result'] = 'failure';
                $response['message'] = $e->getMessage();

                return response()->json($response);
            }

        } elseif ($payment_option == 'PayPal') {
            $paid_or_not = 1;
            $this->insertCheckOut($paid_or_not,$order_id, $totalAmount, $shopper->id, $payment_option, $payment_id, $details, $no_of_items, $checkout_date);
            $response['result'] = 'success';
            $response['message'] = 'Payment processed successfully. Thank you and continue shopping.';
            return response()->json($response);
        } elseif ($payment_option == 'upon-delivery') {
            $paid_or_not = 0;
            $this->insertCheckOut($paid_or_not,$order_id, $totalAmount, $shopper->id, $payment_option, $payment_id, $details, $no_of_items, $checkout_date);
            $response['result'] = 'success';
            $response['message'] = 'Check out completed successfully. Thank you and continue shopping.';
            return response()->json($response);
        }


    }

    //Record checkout transaction in database
    public function insertCheckOut($paid_or_not,$order_id, $totalAmount, $shopper_id, $payment_option, $payment_id, $details_new, $no_of_items, $checkout_date)
    {

        $checkout = new \App\Checkout();
        $checkout->order_id = $order_id;
        $checkout->totalAmount = $totalAmount;
        $checkout->shopper_id = $shopper_id;
        $checkout->payment_option = $payment_option;
        $checkout->payment_id = $payment_id;
        $checkout->no_of_items = $no_of_items;
        $checkout->check_out_date = $checkout_date;
        $checkout->paid_or_not = $paid_or_not;
        $checkout->order_status = 1;
        $checkout->save();

        for ($i = 0; $i < sizeof($details_new); $i++) {
            $checkoutDetails = new \App\CheckoutDetails();
            $checkoutDetails->order_id = $checkout->id;
            $checkoutDetails->product_id = $details_new[$i]['product_id'];
            $checkoutDetails->quantity = $details_new[$i]['quantity'];
            if ($details_new[$i]['siz_e']) {
                $checkoutDetails->siz_e = $details_new[$i]['siz_e'];
            }
            if ($details_new[$i]['color']) {
                $checkoutDetails->color = $details_new[$i]['color'];
            }
            $checkoutDetails->save();
        }

    }

    //Get items in an order
    public function getOrderItems(Request $request)
    {
        $order_id = $request->input('order_id');
        $orderDetails = \App\CheckoutDetails::leftJoin('products', 'checkout_details.product_id', '=', 'products.id')
            ->leftJoin('category', 'products.category_id', '=', 'category.id')
            ->select('products.rate', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription',  'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
            ->where('order_id', $order_id)
            ->get();

        $response['result'] = "success";
        $response['message'] = "Order items retieved successfully";
        $response['orderDetails'] = $orderDetails;
        return response()->json($response);
    }


}
