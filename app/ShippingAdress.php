<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAdress extends Model {

    protected $fillable = [];
    protected $table = 'shipping_address';

}
