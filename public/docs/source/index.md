---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_13d674a3cdc87f8b7f1e16bbb2892d19 -->
## api/sm/getFeatured

> Example request:

```bash
curl -X GET "http://localhost/api/sm/getFeatured" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getFeatured",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/sm/getFeatured`

`HEAD api/sm/getFeatured`


<!-- END_13d674a3cdc87f8b7f1e16bbb2892d19 -->

<!-- START_eb89bb76c91936870684c881c1c48fe4 -->
## api/sm/getProductsNew

> Example request:

```bash
curl -X GET "http://localhost/api/sm/getProductsNew" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getProductsNew",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/sm/getProductsNew`

`HEAD api/sm/getProductsNew`


<!-- END_eb89bb76c91936870684c881c1c48fe4 -->

<!-- START_b6bfbbd5c03b0f6cf83d3cb0ffcf8ce1 -->
## api/sm/getCategory

> Example request:

```bash
curl -X GET "http://localhost/api/sm/getCategory" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getCategory",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/sm/getCategory`

`HEAD api/sm/getCategory`


<!-- END_b6bfbbd5c03b0f6cf83d3cb0ffcf8ce1 -->

<!-- START_f94aec3d39d1e93e067a062cc05504f9 -->
## api/sm/getProductsByCategory

> Example request:

```bash
curl -X GET "http://localhost/api/sm/getProductsByCategory" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getProductsByCategory",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/sm/getProductsByCategory`

`HEAD api/sm/getProductsByCategory`


<!-- END_f94aec3d39d1e93e067a062cc05504f9 -->

<!-- START_fbcf05028e63dbcd0a9e52be55169d8b -->
## api/sm/getBrands

> Example request:

```bash
curl -X GET "http://localhost/api/sm/getBrands" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getBrands",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/sm/getBrands`

`HEAD api/sm/getBrands`


<!-- END_fbcf05028e63dbcd0a9e52be55169d8b -->

<!-- START_afbbe87d114f7e87ed54b2aed4d80ee9 -->
## api/sm/getAds

> Example request:

```bash
curl -X GET "http://localhost/api/sm/getAds" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getAds",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/sm/getAds`

`HEAD api/sm/getAds`


<!-- END_afbbe87d114f7e87ed54b2aed4d80ee9 -->

<!-- START_b4d117dfac302c3c9de0abc00f66e61f -->
## api/sm/getProductsByBrand

> Example request:

```bash
curl -X POST "http://localhost/api/sm/getProductsByBrand" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getProductsByBrand",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/sm/getProductsByBrand`


<!-- END_b4d117dfac302c3c9de0abc00f66e61f -->

<!-- START_23a36d15c3543a50ce9b073f24f5dcbe -->
## api/sm/getProductImages

> Example request:

```bash
curl -X POST "http://localhost/api/sm/getProductImages" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/getProductImages",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/sm/getProductImages`


<!-- END_23a36d15c3543a50ce9b073f24f5dcbe -->

<!-- START_82997bcf584d659415b17697751099f6 -->
## api/sm/checkout

> Example request:

```bash
curl -X POST "http://localhost/api/sm/checkout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/checkout",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/sm/checkout`


<!-- END_82997bcf584d659415b17697751099f6 -->

<!-- START_aecb9bd383d36a1ff17c2cd22661a794 -->
## api/sm/orderhistory

> Example request:

```bash
curl -X POST "http://localhost/api/sm/orderhistory" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/sm/orderhistory",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/sm/orderhistory`


<!-- END_aecb9bd383d36a1ff17c2cd22661a794 -->

