<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//get featured
Route::get('/sm/getFeatured', 'MobileController@getFeatured');
//get Category
Route::get('/sm/getCategory', 'MobileController@getCategorys');
//get categorys and prosucts(10 products per category)
Route::get('/sm/getCategoryProducts', 'MobileController@getCategorysProducts');
//get products by category
Route::post('sm/getProductsByCategory', 'MobileController@getProductsByCategory');
//get Adds
Route::get('sm/getAds', 'MobileController@getAdverts');
//products images
Route::post('sm/getProductImages', 'MobileController@getProductImages');
//check out
Route::post('sm/checkout', 'MobileController@checkout');
//order history
Route::post('sm/orderhistory', 'MobileController@orderHistory');
//Save Shopper to database
Route::post('sm/saveProfile', 'MobileController@saveProfile');
//Update Shopper to database
Route::post('sm/updateShopper', 'MobileController@updateShopper');
//Update profile pic
Route::post('sm/updateProfilePic', 'MobileController@updateProfilePic');
//Shoper shipping address
Route::post('sm/getShipingBilingAddress', 'MobileController@getShippingAndBillingAddress');
//Get addresses
Route::post('sm/getAddress', 'MobileController@getAddress');
//Add a shiping address
Route::post('sm/addShippingAddress', 'MobileController@saveShippingAdress');
//Add a billing address
Route::post('sm/saveBillingAdress', 'MobileController@saveBillingAdress');
//Get similar products
Route::post('sm/getSimilarProducts', 'MobileController@similarProducts');
//order history
Route::post('sm/history-order-details', 'MobileController@orderHistoryProducts');
//Order Status
Route::post('sm/order-status', 'MobileController@orderStatus');
//Get sliders
Route::get('sm/slider', 'MobileController@slider');
//search products
Route::post('sm/search-products', 'MobileController@searchProducts');
//make payment
Route::post('sm/make-payment', 'MobileController@checkOut');
//get items in an order
Route::post('sm/get-Order-Items', 'MobileController@getOrderItems');
